<?php
/*
Template Name: Synchronous XML Feed Template
*/
?>

<?php while (have_posts()) : the_post(); ?>
    <?php get_template_part('templates/page', 'header'); ?>
    <?php get_template_part('templates/content', 'page'); ?>

    <?php $xml = new xmlFeeds(); ?>

    <div class="col-md-4">
        <?php $xml->output_feed('top_stories'); ?>
    </div>
    <div class="col-md-4">
        <?php $xml->output_feed('business'); ?>
    </div>
    <div class="col-md-4">
        <?php $xml->output_feed('technology'); ?>
    </div>



<?php endwhile;
