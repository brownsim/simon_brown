<?php

class xmlFeeds {
    private $feeds = array(
        "top_stories" => "http://feeds.bbci.co.uk/news/rss.xml",
        "business"  => "http://feeds.bbci.co.uk/news/business/rss.xml",
        "technology" => "http://feeds.bbci.co.uk/news/technology/rss.xml"
    );

    public function output_feed($feed) {

        $xml = file_get_contents($this->feeds[$feed]);

        $xml = simplexml_load_string($xml);

//        var_dump($xml);
//        var_dump($xml->channel->item[0]);

        echo $this->feed_title($xml->channel);
        echo $this->feed_item($xml->channel, 5);

    }

    private function feed_title($xml) {

        $html = "<h2>".$xml->title."</h2>";
        $html .= "<div>".$xml->description."</div>";

        return $html;

    }

    private function feed_item($xml, $no_items) {

        $html = '';

        for ($x = 1; $x <= $no_items; $x++) {

            $html .= "<div class='article'>";
            $html .= "<h3>";
            $html .= "<a href='". $xml->item[$x]->link."'>";
            $html .= $xml->item[$x]->title;
            $html .= "</a></h3>";
            $html .= "<p>".$xml->item[$x]->description."</p>";
            $html .= "</div>";

        }

        return $html;
    }
}

add_action('wp_head','set_ajaxurl');
function set_ajaxurl() {
    ?>
    <script type="text/javascript">
        var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
    </script>
    <?php
}

add_action( 'wp_ajax_bbc_ajax_load', 'bbc_ajax_load' );

function bbc_ajax_load($no_items) {
    $i = $no_items;
    return $no_items;
}