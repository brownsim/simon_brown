<?php
/*
Template Name: ASynchronous XML Feed Template
*/
?>

<?php while (have_posts()) : the_post(); ?>
    <?php get_template_part('templates/page', 'header'); ?>
    <?php get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>

<div id="ajax-load-articles">
</div>